﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LoginAPI.Models;
namespace LoginAPI.Controllers
{
    public class LoginController : ApiController
    {
        //For user login   
        [Route("Api/Login/UserLogin")]
        [HttpPost]
        public Response Login(Login Lg)
        {
            EmployeeEntities DB = new EmployeeEntities();
            Employeemaster Em = new Employeemaster();
            Em = DB.Employeemasters.Where(jr => jr.UserName == Lg.UserName  &&  jr.Password == Lg.Password).FirstOrDefault();
            //var objUser = (from dr in DB.Employeemasters
            //               where dr.UserName == Lg.UserName && dr.Password == Lg.Password
            //               select new
            //               {
            //                   dr.UserId,
            //                   dr.Status,
            //                   dr.IsApporved
            //               }).ToList();
         //   return new Response { Status = "Success", Message = Lg.UserName };
          if (Em == null)
                return new Response { Status = "Invalid", Message = "Invalid User." };
            //var Obj = DB.Usp_Login(Lg.UserName, Lg.Password).ToList<Usp_Login_Result>().FirstOrDefault();
            //if (objUser.Status == null)
            //    return new Response { Status = "Invalid", Message = "Invalid User." };
            if (Em.Status == 0)
                return new Response { Status = "Inactive", Message = "User Inactive." };
            else
                return new Response { Status = "Success", Message = Lg.UserName };
        }
        //For new user Registration  
        [Route("Api/Login/createcontact")]
        [HttpPost]
        public object createcontact(Registration Lvm)
        {
            try
            {
                EmployeeEntities db = new EmployeeEntities();
                Employeemaster Em = new Employeemaster();
                if (Em.UserId == 0)
                {
                    Em.UserName = Lvm.UserName;
                    Em.LoginName = Lvm.LoginName;
                    Em.Password = Lvm.Password;
                    Em.Email = Lvm.Email;
                    Em.ContactNo = Lvm.ContactNo;
                    Em.Address = Lvm.Address;
                    //      Em.IsApporved = Lvm.IsApporved;
                    //      Em.Status = Lvm.Status;
                    Em.IsApporved = 1;
                    Em.Status = 1;
                    db.Employeemasters.Add(Em);
                    db.SaveChanges();
                    return new Response
                    { Status = "Success", Message = "SuccessFully Saved." };
                }
            }
            catch (Exception)
            {
                throw;
            }
            return new Response
            { Status = "Error", Message = "Invalid Data." };
        }
    }
}